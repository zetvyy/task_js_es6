// Destructuring Array
const players = ["Ansu Fati", "Pique", "Jordi Alba", "Dembele", "Memphis", "Frenkie"];
const [player1, player2] = players;

console.log(player2);

// Destructuring Object
const newPlayer = {
  name: "Memphis Depay",
  age: 26,
  nationaly: "Netherlands",
  position: "Striker, Winger",
  statistic: {
    goal: 27,
    assist: 14,
    games: 56
  }
};

const {
  name,
  age,
  nationaly,
  statistic: { goal, assist }
} = newPlayer;
console.log(name, goal);

// Template Literals
console.log(`${name} is the new FC Barcelona Player this season`);

// Ternary Operator
const number = 20 - 5 > 10 + 3 ? "true" : "false";
console.log(number);

// Array of object
const barcaPlayer = [
  {
    name: "Memphis Depay",
    age: 26,
    nationaly: "Netherlands"
  },
  {
    name: "Ansu Fati",
    age: 18,
    nationaly: "Spain"
  },
  {
    name: "Frenkie De jong",
    age: 23,
    nationaly: "Netherlands"
  },
  {
    name: "Sergio",
    age: 30,
    nationaly: "Spain"
  },
  {
    name: "Sergino Dest",
    age: 20,
    nationaly: "USA"
  },
  {
    name: "Jordi Alba",
    age: 30,
    nationaly: "Spain"
  }
];

barcaPlayer.map(player => {
  console.log(player);
});

let filterByNationaly = barcaPlayer.filter(player => player.nationaly === "Spain");
console.log(filterByNationaly);

let findPlayer = barcaPlayer.find(player => player.nationaly === "USA");
console.log(findPlayer);
